﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio2POO
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
            
        }
        
        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            //se manda mensaje de bienvenidda
            lblBienvenida.Text = "Bienvenido " + frmLogin.usuario.Nombre;
        }

        private void btnCambiarContra_Click(object sender, EventArgs e)
        {
            frmCambiarContrasenia frm = new frmCambiarContrasenia();
            this.Hide();
            frm.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            frmLogin frm = new frmLogin();
            this.Hide();
            frm.Show();
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            frmConsultarInventario frm = new frmConsultarInventario();
            this.Hide();
            frm.Show();
        }

        private void btnVentaNueva_Click(object sender, EventArgs e)
        {
            frmVentaNueva frm = new frmVentaNueva();
            this.Hide();
            frm.Show();
        }
    }
}
