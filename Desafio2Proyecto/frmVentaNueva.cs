﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio2POO
{
    public partial class frmVentaNueva : Form
    {
        public frmVentaNueva()
        {
            InitializeComponent();
        }
        //obteniendo lista de Productos con precio
        List<Productos> listaProductos = frmLogin.productos;
        List<string> arregloCompra = new List<string>();
        List<int> arregloCodigo = new List<int>();
        List<int> arregloCantidad = new List<int>();
        string cadenaCompra, cadenaSpliteada;
        double totalFactura =0.00;
        double totalDescuento = 0.00;

        //método para actualizar el grid 
        private void actualizarGrid()
        {
            dgvVentaNueva.DataSource = null;
            dgvVentaNueva.DataSource = listaProductos;
            dgvVentaNueva.Columns[2].Visible = false;
        }
        //método de limpieza para los campos
        private void limpieza()
        {
            lstResultado.Items.Clear();
            lstIteraciones.Items.Clear();
            lblFactura.Text = "";
            totalFactura = 0.00;
            arregloCompra.Clear();
            arregloCantidad.Clear();
            arregloCodigo.Clear();
        }

        //Cargando los datos en la pestaña
        private void frmVentaNueva_Load(object sender, EventArgs e)
        {
            //se limpia el dgv
            actualizarGrid();
        }
        //Botón de regreso al menú principal
        private void button2_Click(object sender, EventArgs e)
        {
            frmPrincipal frm = new frmPrincipal();
            frm.Show();
            this.Hide();
        }
        //Botón de compra 
        private void btnComprar_Click(object sender, EventArgs e)
        {
            limpieza();
            //Validando entrada de texto
            Productos cadena = new Productos();
            if (cadena.esCadenaCompra(txtIngresoVenta.Text) == true)
            {
                cadenaCompra = txtIngresoVenta.Text;
                arregloCompra = cadenaCompra.Split(',').ToList();
                cadenaSpliteada = String.Join(",", arregloCompra.ToArray());
                //Validando campos no ingresados
                if (cadena.esCadenaVacia(cadenaSpliteada) == true)
                {
                    MessageBox.Show("Existen espacios en la lista");
                    
                }
                else
                {
                    //Validando una coma al final
                    if (cadena.esTerminaEnComa(cadenaSpliteada) == false)
                    {

                        for (int i = 0; i < arregloCompra.Count; i++)
                        {
                            //Separando los campos con para seleccionar código
                            if ((i % 2) == 0)
                            {
                                arregloCodigo.Add(Convert.ToInt32(arregloCompra[i]));
                            }
                            //Separando los campos con para seleccionar cantidad
                            else
                            {
                                arregloCantidad.Add(Convert.ToInt32(arregloCompra[i]));
                            }
                        }
                        for (int i = 0; i < arregloCodigo.Count; i++)
                        {

                            if (arregloCodigo[i] > listaProductos.Count)
                            {
                                MessageBox.Show("El código del producto ingresado no exite: código= " + arregloCodigo[i]);
                            }
                            foreach (Productos item in listaProductos.Where(elCodigo => elCodigo.Codigo == arregloCodigo[i]))
                            {
                                //Validación de existencia de productos
                                if (item.Cantidad == 0)
                                {
                                    lstIteraciones.Items.Add("No contamos con existencia del producto: '" + Convert.ToString(item.Producto) + "'");
                                    break;
                                }
                                //Validación para vender cantidad máxima en stock
                                if (item.Cantidad < arregloCantidad[i] && item.Cantidad > 0)
                                {
                                    lstIteraciones.Items.Add("- No se tiene esa cantidad en stock disponible de '" + item.Producto + "' pero se le otorgará la máxima disponible");
                                    arregloCantidad[i] = item.Cantidad;
                                }
                                //Almacenando total de factura por cada producto
                                totalFactura += Convert.ToDouble(item.Precio) * Convert.ToDouble(arregloCantidad[i]);
                                lstResultado.Items.Add(item.Producto + " --- " + arregloCantidad[i] + " x " + " $" + item.Precio + " = " + " $" + Convert.ToDouble(item.Precio) * Convert.ToDouble(arregloCantidad[i]) + "\n");
                                //Restando el valor de compra a la cantidad de producto
                                if (item.Cantidad >= arregloCantidad[i])
                                {
                                    item.Cantidad = item.Cantidad - arregloCantidad[i];
                                }
                                else
                                {
                                    item.Cantidad = 0;
                                }
                                if (totalFactura > 20)
                                {
                                    totalDescuento = totalFactura * 0.03;
                                    totalFactura = totalFactura - totalFactura * 0.03;

                                    lstResultado.Items.Add("Se le aplicó un descuento de: " + totalDescuento);
                                }
                                // busca el valor para el combo de regalo
                                int valorPollo = lstResultado.FindString("pollo");
                                int valorGaseosa = lstResultado.FindString("gaseosa");
                                //verifica que exista el combo
                                if (valorPollo != ListBox.NoMatches && valorGaseosa != ListBox.NoMatches)
                                {
                                    if ((int)dgvVentaNueva.Rows[4].Cells["Cantidad"].Value >=5 )
                                    {
                                        dgvVentaNueva.Rows[4].Cells["Cantidad"].Value = (int)dgvVentaNueva.Rows[4].Cells["Cantidad"].Value - 5;
                                        lstResultado.Items.Add("Por su compra recibe 5 dulces gratis \n");
                                    }
                                    else
                                    {
                                        lstIteraciones.Items.Add("Por su compra recibe 5 dulces gratis pero ya se nos agotaron \n");
                                    }

                                }  
                                //Mostrando total facturado
                                lblFactura.Text = "$" + Convert.ToString(totalFactura);
                            }
                        }
                        

                        //actualizando los datos
                        actualizarGrid();
                        txtIngresoVenta.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Debe ingresar el código seguido de una coma y la cantidad que desea adquirir");
                    }
                }

                }else
            {
                MessageBox.Show("La lista ingresada no es correcta");
            }  
        }
    }
}
