﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Desafio2POO
{
    public class Productos
    {
        private int codigo;
        private string producto;
        private int cantidad;
        private double precio;


        //Patron de validación para números con expresión regular
        Regex regexCadena = new Regex(@"^[\d,\d]{3,}$");
        Regex regexVacio = new Regex(@",,");
        Regex regexTerminar = new Regex(@"[,]$");
        //Constructor clase base
        public Productos()
        {
        }

        public Productos(int codigo, string nombre, int cantidad, double precio)
        {
            this.Codigo = codigo;
            this.Producto = nombre;
            this.Cantidad = cantidad;
            this.Precio = precio;
        }   
        public int Codigo { get => codigo; set => codigo = value; }
        public string Producto { get => producto; set => producto = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double Precio { get => precio; set => precio = value; }

        //Método de validación para cadena
        public bool esCadenaCompra(string cadena)
        {
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(cadena))
            {
                //Valida si es una cadena
                if (regexCadena.IsMatch(cadena))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        //Método de validación para cadena
        public bool esCadenaVacia(string cadena)
        {
                //Valida si es una cadena
                if (regexVacio.IsMatch(cadena))
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }
        //Método de validación para cadena
        public bool esTerminaEnComa(string cadena)
        {
            //Valida si es una cadena
            if (regexTerminar.IsMatch(cadena))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
