﻿
namespace Desafio2POO
{
    partial class frmVentaNueva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvVentaNueva = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnComprar = new System.Windows.Forms.Button();
            this.txtIngresoVenta = new System.Windows.Forms.TextBox();
            this.IngresarValor = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.lstResultado = new System.Windows.Forms.ListBox();
            this.lblFactura = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lstIteraciones = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentaNueva)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvVentaNueva
            // 
            this.dgvVentaNueva.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVentaNueva.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVentaNueva.Location = new System.Drawing.Point(25, 61);
            this.dgvVentaNueva.Name = "dgvVentaNueva";
            this.dgvVentaNueva.ReadOnly = true;
            this.dgvVentaNueva.RowHeadersWidth = 51;
            this.dgvVentaNueva.RowTemplate.Height = 24;
            this.dgvVentaNueva.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVentaNueva.Size = new System.Drawing.Size(562, 248);
            this.dgvVentaNueva.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(125, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(403, 24);
            this.label1.TabIndex = 37;
            this.label1.Text = "Sistema de Ventas Don Diego / Venta Nueva";
            // 
            // btnComprar
            // 
            this.btnComprar.Location = new System.Drawing.Point(451, 350);
            this.btnComprar.Margin = new System.Windows.Forms.Padding(4);
            this.btnComprar.Name = "btnComprar";
            this.btnComprar.Size = new System.Drawing.Size(100, 28);
            this.btnComprar.TabIndex = 40;
            this.btnComprar.Text = "Consultar";
            this.btnComprar.UseVisualStyleBackColor = true;
            this.btnComprar.Click += new System.EventHandler(this.btnComprar_Click);
            // 
            // txtIngresoVenta
            // 
            this.txtIngresoVenta.Location = new System.Drawing.Point(244, 353);
            this.txtIngresoVenta.Margin = new System.Windows.Forms.Padding(4);
            this.txtIngresoVenta.Name = "txtIngresoVenta";
            this.txtIngresoVenta.Size = new System.Drawing.Size(169, 22);
            this.txtIngresoVenta.TabIndex = 39;
            // 
            // IngresarValor
            // 
            this.IngresarValor.AutoSize = true;
            this.IngresarValor.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IngresarValor.Location = new System.Drawing.Point(27, 354);
            this.IngresarValor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IngresarValor.Name = "IngresarValor";
            this.IngresarValor.Size = new System.Drawing.Size(186, 21);
            this.IngresarValor.TabIndex = 38;
            this.IngresarValor.Text = "Ingresar lista de productos";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(25, 466);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 28);
            this.button2.TabIndex = 41;
            this.button2.Text = "Regresar al menú";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lstResultado
            // 
            this.lstResultado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstResultado.BackColor = System.Drawing.SystemColors.Control;
            this.lstResultado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstResultado.Font = new System.Drawing.Font("Myanmar Text", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstResultado.FormattingEnabled = true;
            this.lstResultado.ItemHeight = 30;
            this.lstResultado.Location = new System.Drawing.Point(770, 76);
            this.lstResultado.Name = "lstResultado";
            this.lstResultado.ScrollAlwaysVisible = true;
            this.lstResultado.Size = new System.Drawing.Size(699, 120);
            this.lstResultado.TabIndex = 45;
            // 
            // lblFactura
            // 
            this.lblFactura.AutoSize = true;
            this.lblFactura.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactura.Location = new System.Drawing.Point(1099, 238);
            this.lblFactura.Name = "lblFactura";
            this.lblFactura.Size = new System.Drawing.Size(0, 21);
            this.lblFactura.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(946, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(204, 48);
            this.label2.TabIndex = 47;
            this.label2.Text = "Factura de compra\r\nSupermercado Don Diego";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(946, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 21);
            this.label3.TabIndex = 48;
            this.label3.Text = "Total de factura:";
            // 
            // lstIteraciones
            // 
            this.lstIteraciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstIteraciones.BackColor = System.Drawing.SystemColors.Control;
            this.lstIteraciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstIteraciones.Font = new System.Drawing.Font("Myanmar Text", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstIteraciones.FormattingEnabled = true;
            this.lstIteraciones.ItemHeight = 30;
            this.lstIteraciones.Location = new System.Drawing.Point(608, 283);
            this.lstIteraciones.Name = "lstIteraciones";
            this.lstIteraciones.ScrollAlwaysVisible = true;
            this.lstIteraciones.Size = new System.Drawing.Size(886, 150);
            this.lstIteraciones.TabIndex = 49;
            // 
            // frmVentaNueva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1901, 521);
            this.Controls.Add(this.lstIteraciones);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblFactura);
            this.Controls.Add(this.lstResultado);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnComprar);
            this.Controls.Add(this.txtIngresoVenta);
            this.Controls.Add(this.IngresarValor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvVentaNueva);
            this.Name = "frmVentaNueva";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmVentaNueva";
            this.Load += new System.EventHandler(this.frmVentaNueva_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentaNueva)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvVentaNueva;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnComprar;
        private System.Windows.Forms.TextBox txtIngresoVenta;
        private System.Windows.Forms.Label IngresarValor;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox lstResultado;
        private System.Windows.Forms.Label lblFactura;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstIteraciones;
    }
}