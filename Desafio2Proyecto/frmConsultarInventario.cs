﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio2POO
{
    public partial class frmConsultarInventario : Form
    {
        public frmConsultarInventario()
        {
            InitializeComponent();
        }
        //obteniendo lista de ventas
        List<Productos> ventas = frmLogin.productos;
        //declarando lista que contendra el filtrado de inventario
        List<Productos> inventarioFiltrado = new List<Productos>();
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //se limpia el dgv
            dgvProductos.DataSource = null;
            inventarioFiltrado.Clear();

            string dato = txtDato.Text;
            int codigo;
            //se verifica si se escribio algo en el txt
            if (dato != null && dato != "")
            {
                //se verifica si es codigo o nombre de producto
                bool esCodigo = int.TryParse(dato, out codigo);
                //se busca a partir del dato escrito
                for (int i = 0; i < ventas.Count(); i++)
                {
                    if (!esCodigo && ventas[i].Producto == dato.ToLower())
                    {
                        inventarioFiltrado.Add(ventas[i]);
                    }
                    if (esCodigo && ventas[i].Codigo == codigo)
                    {
                        inventarioFiltrado.Add(ventas[i]);
                    }
                }
                //en caso que se solicite todo, se muestra la lista completa
                if (!esCodigo && dato.ToLower() == "todos")
                {
                    inventarioFiltrado = ventas.ToList();
                }
            }
            else
            {
                MessageBox.Show("Dato de búsqueda no válido", "Error");
            }
            //se manda el inventario filtrado al dgv
            dgvProductos.DataSource = inventarioFiltrado;
            dgvProductos.Columns[3].Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmPrincipal frm = new frmPrincipal();
            frm.Show();
            this.Hide();
        }
    }
}
