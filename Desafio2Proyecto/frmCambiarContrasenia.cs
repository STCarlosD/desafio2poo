﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio2POO
{
    public partial class frmCambiarContrasenia : Form
    {
        public frmCambiarContrasenia()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            regresarMenu();
        }

        private void btnCambiar_Click(object sender, EventArgs e)
        {
            string nuevaContra = txtNuevaContra.Text;
            //se evalua si se ha escrito una contraseña o no
            if (nuevaContra != null && nuevaContra != "" && nuevaContra != " ")
            {
                //se verifica cual usuario esta en sesion y a partir de eso se cambia la contraseña
                if (frmLogin.usuario.Nombre == "admin")
                {
                    frmLogin.passAdmin = nuevaContra;
                }else if (frmLogin.usuario.Nombre == "vendedor")
                {
                    frmLogin.passVendedor = nuevaContra;
                }else if (frmLogin.usuario.Nombre == "invitado")
                {
                    frmLogin.passInvitado = nuevaContra;
                }
                //se direcciona al menu
                regresarMenu();
            }
            else
            {
                txtNuevaContra.Clear();
                MessageBox.Show("Dato para contraseña no válido", "Error");
            }
        }

        private void frmCambiarContrasenia_Load(object sender, EventArgs e)
        {
            //label con el nombre de usuario en sesion
            lblTexto.Text = "Nueva contraseña para " + frmLogin.usuario.Nombre;
        }

        void regresarMenu()
        {
            frmPrincipal frm = new frmPrincipal();
            frm.Show();
            this.Hide();
        }
    }
}
