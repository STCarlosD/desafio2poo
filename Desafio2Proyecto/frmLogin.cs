﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio2POO
{
    public partial class frmLogin : Form
    {
        //declarando objetos para los usuarios
        Usuario admin;
        Usuario vendedor;
        Usuario invitado;

        internal static Usuario usuario;

        //se declara lista de productos
        internal static List<Productos> productos = new List<Productos>();
        public frmLogin()
        {
            InitializeComponent();
            //creando los objetos para los usuarios
            admin = new Usuario("admin", passAdmin);
            vendedor = new Usuario("vendedor", passVendedor);
            invitado = new Usuario("invitado", passInvitado);
            productos.Clear();
            //se agregan productos a la lista
            productos.Add(new Productos(1, "huevos", cantidades[0], precio[0]));
            productos.Add(new Productos(2, "pollo", cantidades[1], precio[1]));
            productos.Add(new Productos(3, "aceite", cantidades[2], precio[2]));
            productos.Add(new Productos(4, "fosforos", cantidades[3], precio[3]));
            productos.Add(new Productos(5, "dulces", cantidades[4], precio[4]));
            productos.Add(new Productos(6, "margarina", cantidades[5], precio[5]));
            productos.Add(new Productos(7, "jabon", cantidades[6], precio[6]));
            productos.Add(new Productos(8, "carne", cantidades[7], precio[7]));
            productos.Add(new Productos(9, "gaseosa", cantidades[8], precio[8]));
            productos.Add(new Productos(10, "desechables", cantidades[9], precio[9]));
        }

        //array para guardar las cantidades de productos
        internal static int[] cantidades = new int[10] { 30, 10, 50, 200, 500, 30, 25, 35, 200, 800 };
        //array para guardar las cantidades de productos
        internal static double[] precio = new double[10] { 0.10, 5.00, 3.00, 0.5, 0.80, 0.30, 2.25, 2.75, 1.80, 3.25 };

        //contraseñas para los usuarios
        internal static string passAdmin = "admin123";
        internal static string passVendedor = "vendedor123";
        internal static string passInvitado = "invitado123";

        private void tbnAcceder_Click(object sender, EventArgs e)
        {
            usuario = new Usuario(txtUsuario.Text, txtContra.Text);
            bool bandera = false;
            //verificando si las credenciales coinciden con las que se tienen
            if (usuario.Nombre == admin.Nombre && usuario.Contrasenia == admin.Contrasenia)
            {
                bandera = true;
            }else if (usuario.Nombre == vendedor.Nombre && usuario.Contrasenia == vendedor.Contrasenia)
            {
                bandera = true;
            }
            else if (usuario.Nombre == invitado.Nombre && usuario.Contrasenia == invitado.Contrasenia)
            {
                bandera = true;
            }
            //si no coinciden se manda mensaje de error, caso contrario se manda al formulario principal
            if (bandera)
            {
                frmPrincipal frm = new frmPrincipal();                
                this.Hide();
                frm.Show();                
            }
            else
            {
                txtContra.Clear();
                txtUsuario.Clear();
                lblMensaje.Text = "Credenciales incorrectas.";
            }
        }
    }
}
